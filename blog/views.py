import os
from io import StringIO
from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage
from django.core.mail import send_mail, EmailMessage
from django.core.mail import EmailMultiAlternatives
from django.shortcuts import render, redirect
# Create your views here.
from django.utils import timezone

from rest_framework import viewsets

from blog.forms import PostForm
from blog.models import Post
from category.forms import CategoryForm
from category.models import Category
from public_python import settings
from public_python.serializers import PostSerializers, CategorySerializers, LargeResultsSetPagination
from public_python.settings import MAPBOX_KEY, BASE_DIR


class PostViewSet(viewsets.ModelViewSet):
    posts = Post.objects.all().order_by('-created_date')

    queryset = posts
    serializer_class = PostSerializers
    pagination_class = LargeResultsSetPagination


class CategoryViewSet(viewsets.ModelViewSet):
    categories = Category.objects.all().order_by('-created_date')

    queryset = categories
    serializer_class = CategorySerializers


def post_list(request):
    return render(request, 'post/post.html')


def post(request, id):
    post = Post.objects.get(id=id)
    attach = StringIO()
    mapToken = MAPBOX_KEY
    if request.method == "POST":
        subject = 'Nowy aplikant do twojej firmy ze strony technology-work.com '
        text_content = 'Oto link do mojego repozytorium. Ze strony https://technology-work.com' \
                       '<p>Repozytorium <a class="btn btn-danger" href="' + request.POST.get(
            'link') + '">' + request.POST.get('link') + '</a></p>'
        myfile = request.FILES['fileCv']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        msg = EmailMessage(subject, text_content, request.POST.get('email'), [post.contactEmail])
        msg.attach_file(os.path.join(BASE_DIR, 'public', 'media/'+filename), request.FILES.get('fileCv').content_type)
        msg.send()

    return render(request, 'post/show.html', {
        'post': post,
        'mapToken': mapToken

    })


@login_required
def post_create(request):
    form = PostForm()
    categories = Category.objects.all()
    if request.method == "POST":
        form = PostForm(request.POST)
        post = form.save(commit=False)
        post.author = request.user
        post.contactEmail = request.POST.get('contactEmail') or request.user.email
        post.published_date = timezone.now()
        post.save()
        for data in form.data.getlist("category[]"):
            post.category.add(data)

        return redirect('post_list')

    return render(request, 'post/create.html', {'form': form, 'categories': categories})
