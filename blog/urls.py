from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from blog import views
from blog.views import PostViewSet, CategoryViewSet

router = DefaultRouter()
router.register(r'posts', PostViewSet)
router.register(r'categories', CategoryViewSet)

urlpatterns = [
                  url(r'^ckeditor/', include('ckeditor_uploader.urls')),
                  path('', views.post_list, name='post_list'),
                  path('api/post',  include(router.urls)),
                  path('', include(router.urls)),
                  path('<int:id>', views.post, name='post'),
                  path('new/post', views.post_create, name='post_create'),
                  # ]
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
