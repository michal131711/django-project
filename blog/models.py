from ckeditor.fields import RichTextField
from django.db import models
from mapbox_location_field.models import LocationField, AddressAutoHiddenField

# Create your models here.
from django.utils import timezone

from category.models import Category
from country.models import Country
from user.models import User


class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    typeOffer = models.CharField(max_length=200)
    experience_level = models.CharField(max_length=200)
    vote = models.IntegerField(null=True)
    companyName = models.CharField(max_length=200)
    companySpecialization = models.CharField(max_length=200)
    priceOffer = models.DecimalField(max_digits=30, decimal_places=15)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    location = LocationField()
    address = AddressAutoHiddenField()
    currency = models.CharField(max_length=30)
    contactPhone = models.CharField(max_length=30)
    contactEmail = models.CharField(max_length=30, null=True)
    active = models.BooleanField(default=0)
    text = RichTextField()
    created_date = models.DateTimeField(
        default=timezone.now)
    published_date = models.DateTimeField(
        blank=True, null=True)
    category = models.ManyToManyField(Category, blank=True, related_name='category')

    class Meta:
        db_table = 'Offer'
        ordering = ('title',)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title
