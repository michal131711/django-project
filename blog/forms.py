from ckeditor.fields import RichTextField
from django import forms
from .models import Post
from mapbox_location_field.forms import AddressAutoHiddenField


class PostForm(forms.ModelForm):
    address = AddressAutoHiddenField(required=False)
    text = RichTextField()
    contactEmail = forms.EmailField(required=False)

    class Meta:
        model = Post
        fields = ('title', 'typeOffer', 'companyName', 'experience_level', 'companySpecialization', 'priceOffer',
                  'country', 'text', 'location', 'address', 'currency', 'contactPhone', 'contactEmail')
