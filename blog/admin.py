from django.contrib import admin

# Register your models here.
from blog.models import Post


class BlogAdmin(admin.ModelAdmin):
    fields = ['author', 'title', 'companyName', 'companySpecialization', 'text', 'priceOffer', 'country', 'city', 'address',
              'numberBuild', 'numberHome', 'postCode', 'currency', 'contactPhone', 'contactEmail', 'category', 'active']

admin.site.register(Post, BlogAdmin)
