from django.db import models

# Create your models here.
from django.utils import timezone


class Country(models.Model):
    name = models.CharField(max_length=200)
    active = models.BooleanField(default=0)
    created_date = models.DateTimeField(
        default=timezone.now)
    published_date = models.DateTimeField(
        blank=True, null=True)

    class Meta:
        db_table = 'Country'
        ordering = ('name',)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.name
