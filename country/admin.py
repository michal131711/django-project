from django.contrib import admin

# Register your models here.
from country.models import Country


class CountryAdmin(admin.ModelAdmin):
    fields = ['name', 'active']

admin.site.register(Country, CountryAdmin)