from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.hashers import make_password, check_password
from django.core.mail import EmailMultiAlternatives, send_mail
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
# Create your views here.
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone

from user.Security import EmailBackend
from user.form import LoginForm
from user.models import User


def register_user(request):
    if request.method == "POST":
        user = User()
        hashed_pwd = make_password(request.POST.get('password'))
        check_password(request.POST.get('password2'), hashed_pwd)
        user.username = request.POST.get('username')
        user.email = request.POST.get('email')
        user.is_staff = True
        user.active = request.POST.get('active', True) == 'on'
        user.password = hashed_pwd
        user.published_date = timezone.now()
        user.save()
        user = authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
        login(request, user)

        return redirect('post_list')

    return render(request, 'user/register.html', {})


def login_user(request):
    if request.method == "POST":
        login_form = LoginForm(request.POST)
        if not login_form.is_valid():
            username = request.POST.get('username')
            password = request.POST.get('password')
            try:
                userLogin = EmailBackend.authenticate(request, username=username, password=password)
                if not userLogin:
                    messages.error(request, 'Nie poprawny email bądż hasło')

                    return HttpResponseRedirect(reverse('login_user'))
                else:
                    login(request, userLogin, backend='django.contrib.auth.backends.ModelBackend')

                    return redirect('post_list')
            except:
                return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

    else:
        login_form = LoginForm()

    return render(request, 'user/logowanie.html', {'login_form': login_form})


def send_password(request):
    if request.method == "POST":
        try:
            email = request.POST.get('email')
            user = User.objects.filter(email=email)
            if not user:
                messages.warning(request, "Ten email nie istnieje w naszej bazie: " + email + ". Zarejestruj się !!")

                return redirect('register_user')
            else:
                subject, from_email, to = 'Nowe hasło', 'michal13171@wp.pl', email
                text_content = 'Kliknij i zmień swoje stare hasło.'
                html_content = render_to_string('user/mail_template.html', {'user': user})
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()

                return redirect('login_user')
        except User.DoesNotExist:
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/user/logowanie'))
    else:
        return render(request, 'user/send_new_password.html')


def reset_password(request, id):
    user = User.objects.get(id=id)
    if request.method == "POST":
        hashed_pwd = make_password(request.POST.get('password'))
        check_password(request.POST.get('password2'), hashed_pwd)
        user.password = hashed_pwd
        user.published_date = timezone.now()
        user.save()

        return redirect('login_user')

    return render(request, 'user/resetPassword.html', {'user': user})


def account_user(request, id):
    user = User.objects.get(id=id)
    if request.method == "POST":
        hashed_pwd = make_password(request.POST.get('password'))
        check_password(request.POST.get('password2'), hashed_pwd)
        user.username = request.POST.get('username')
        user.first_name = request.POST.get('first_name')
        user.last_name = request.POST.get('last_name')
        user.email = request.POST.get('email')
        user.password = hashed_pwd
        user.published_date = timezone.now()
        user.save()
        login(request, user, backend='django.contrib.auth.backends.ModelBackend')

        return redirect('post_list')
    return render(request, 'user/account.html')


def contact(request):
    if request.method == 'POST':
        send_mail(request.POST.get('title'), request.POST.get('desc'),
                  request.POST.get('email'), ['michal131711@gmail.com', request.POST.get('email')])

    return render(request, 'contact.html')
