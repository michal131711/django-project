from django.utils.translation import gettext_lazy as _
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.core.exceptions import ValidationError

from user.models import User


class LoginForm(AuthenticationForm):
    username = forms.EmailField(label=_("Wpisz e-mail"), widget=forms.EmailInput(attrs={'autofocus': True,'class': 'form-control'}))
    password = forms.CharField(
        label=_("Hasło"),
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
    )

    def clean(self):
        email = self.cleaned_data.get('email')
        if User.objects.filter(email=email).exists():
            raise ValidationError("Email exists")
        return self.cleaned_data
