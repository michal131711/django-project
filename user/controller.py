from django.conf.urls import url, include as incOAuth2
from django.contrib.auth import views as auth_views
from django.urls import path

# from blog.views import post_list
from user import views

urlpatterns = [
    path('Rejestracja', views.register_user, name='register_user'),
    path('logowanie', views.login_user, name='login_user'),
    path('reset/<int:id>', views.reset_password, name='reset_password'),
    path('account/<int:id>', views.account_user, name='account_user'),
    url('logout', auth_views.LogoutView.as_view(template_name='post/post.html'), name='logout'),
    # url(r'^$', post_list, name='home'),
    url(r'^login/$', auth_views.auth_login, name='login'),
    path('reset', views.send_password, name='sendPassword'),
    path('contact', views.contact, name='contact'),
    url(r'^oauth/', incOAuth2('social_django.urls', namespace='social')),  # <--
]
