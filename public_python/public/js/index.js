/**
 * @param url
 * @param method
 * @param options
 * @returns
 */
let ajax = (url, method, options) => {
    if (options !== undefined) {
        var {params = {}, data = {}} = options
    } else {
        params = data = {}
    }

    return new Promise((resolve, reject) => {
        axios({
            url,
            method,
            params,
            data
        }).then(res => {
            resolve(res)
        }, res => {
            reject(res)
        });
    })
};

new Vue({
    delimiters: ['[[', ']]'],
    el: '#app',
    data() {
        return {
            msg: '',
            formData: {
                title: '',
                companyName: '',
                companySpecialization: '',
                priceOffer: '',
                country: '',
                text: '',
                address: '',
                currency: '',
                contactPhone: '',
                contactEmail: ''
            },
            posts: [],
            pagin_posts: [],
            categories: [],
            per_size: [],
        }
    },
    methods: {
        fetchAllPosts() {
            fetchPosts('posts', 'get', null, null).then(res => {
                this.pagin_posts = res.config.data;
                this.posts = res.data;
            }).catch((e) => {
                console.log(e)
            })
        },
        fetchAllCategories() {
            fetchCategories('get', null, null).then(res => {
                this.categories = res.data;
            }).catch((e) => {
                console.log(e)
            })
        },
        pagination_last_first_element(index) {
            fetchPosts(index, 'get', null, index).then(res => {
                this.pagin_posts = res.config.data;
                this.posts = res.data;
            }).catch((e) => {
                console.log(e)
            })
        },
        pagination_all_element(index) {
            fetchPosts(`posts/?page=${index}`, 'get', null, index).then(res => {
                this.pagin_posts = res.config.data;
                this.posts = res.data;
            }).catch((e) => {
                console.log(e)
            })
        }
    },
    mounted() {
        this.fetchAllPosts();
        this.fetchAllCategories();
        this.pagination_last_first_element(index);
        this.pagination_all_element(index);
    }
});

Vue.directive('range', {
    inserted: function (el) {
        for (let i = 0; i < el; i++) {
            el.push(i);
        }
    }
});

Vue.filter('formatCustomDate', function (value) {
    if (value) {
        return moment(String(value)).format('YYYY-MMMM-DD')
    }
});

function fetchPosts(url, method, params, data) {
    if (method !== 'post') {
        return ajax(`${url}`, method, {data})
    } else {
        return ajax(`${url}`, 'get', {})
    }
}

function fetchCategories(method, params, data) {
    return ajax('categories', 'get', {})
}