from django.db import models

# Create your models here.
from django.utils import timezone



class Category(models.Model):
    name = models.CharField(max_length=150)
    active = models.BooleanField(default=1)
    created_date = models.DateTimeField(
        default=timezone.now)
    published_date = models.DateTimeField(
        blank=True, null=True)

    class Meta:
        db_table = 'OfferCategory'
        ordering = ('name',)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.name