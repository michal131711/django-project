from django.contrib import admin

# Register your models here.
from category.models import Category


class CategoryAdmin(admin.ModelAdmin):
    fields = ['name', 'active']

admin.site.register(Category, CategoryAdmin)